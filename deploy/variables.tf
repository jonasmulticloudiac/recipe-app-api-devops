variable "prefix" {
  default = "raadev"
}


variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "jonasmultcloudiac@gmail.com"
}