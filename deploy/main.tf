terraform {
  backend "s3" {
    bucket         = "jois-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-lock"
  }
}

provider "aws" {
  region  = "us-east-2"
  version = "~> 3.25.0"
}


locals {

  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"

  }
}

